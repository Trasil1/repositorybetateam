#ifndef MODEL_H_
#define MODEL_H_

#include "database.h"
#include <iostream>

using namespace std;

// Class to handle the model elements of the program
class Model
{
public:
	// Default constructor
	Model() { mCustomerID = -1; mCurrentDate = 1; }

	// Init
	void init() { mDB.load(); }
	void save() { mDB.save(); }
	void registerCustomer(Customer customer) { mDB.addCustomer(customer); }

	bool checkLogin(string username, string password) 
	{ 
		// If user exists we'll get a non-zero ID
		mCustomerID = mDB.checkLogin(username, password);
		if (mCustomerID != -1)
			return true;

		return false;
	}

	vector<Inventory> getInventories() { return mDB.getInventories(); }
	string getCategoryName(int id) { return mDB.getCategoryName(id); }
	void updateCustomerBalance(int newBalance) { mDB.updateCustomerBalance(mCustomerID, newBalance); }
	vector<Order> getOrders() { return mDB.getOrdersByCustomer(mCustomerID); }
	bool purchase(int SKU, int quantity) { return mDB.processPurchase(mCustomerID, SKU, quantity, mCurrentDate); }

	void updateInventory(int SKU, int price, int quantity) { mDB.updateInventoryUnitPrice(SKU, price); mDB.updateInventoryQuantity(SKU, quantity); }

	void removeInventory(int SKU) { mDB.removeInventory(SKU); }

	vector<Order> getOrdersByCID(int cid) { return mDB.getOrdersByCustomer(cid); }
	vector<Order> getOrdersByDate(int date) { return mDB.getOrdersByDate(date); }

	map<int,Category> getCategories() { return mDB.getCategories(); }

	void addInventory(Inventory inventory) { mDB.addInventory(inventory); }

private:
	int mCustomerID;
	int mCurrentDate;
	Database mDB;
};

#endif /* MODEL_H_ */