#ifndef DATABASE_H_
#define DATABASE_H_

#include <string>
#include <vector>
#include <map>

#include "../core/category.h"
#include "../core/merchant.h"
#include "../core/inventory.h"
#include "../core/order.h"
#include "../core/customer.h"

using std::string;
using std::vector;
using std::map;

class Database
{
public:
	// Default constructor
	Database() { }

	// Public load function
	void load();

	// Function to save vector contents to disk
	void save();

	// Accessor functions
	int checkLogin(string username, string password);
	vector<Order> getOrdersByCustomer(int customerID);
	vector<Order> getOrdersByDate(int date);

	vector<Inventory> getInventories() { return mInventories; }

	string getCategoryName(int id) { return mCategories[id].getName(); }

	map<int,Category> getCategories() { return mCategories; }

	// Returns an Inventory* to an object with matching SKU, the return
	// type is a pointer so that if the SKU isn't found we can return 0 instead of having
	// to worry about throwing exceptions
	Inventory* getInventory(int SKU);

	int getCustomerBalance(int customerID);

	// Add functions to interact with storage
	void addCustomer(Customer customer)    
	{
		// Make sure the ID is valid
		if (mCustomers.empty())
			customer.setID(0);
		else
			customer.setID(mCustomers.back().getID() + 1);

		mCustomers.push_back(customer); 
	}

	void addInventory(Inventory inventory) { mInventories.push_back(inventory); }
	void addOrder(Order order)             { mOrders.push_back(order); }

	// Remove functions
	void removeInventory(int SKU);

	// Stored object modification functions
	bool processPurchase(int customerID, int SKU, int quantity, int date);
	void updateCustomerBalance(int customerID, int newBalance);
	void updateInventoryUnitPrice(int SKU, int newPrice);
	void updateInventoryQuantity(int SKU, int newQuantity);

private:
	// Private loader functions for the individual elements
	void loadCustomers();
	void loadInventories();
	void loadOrders();
	void loadMerchants();
	void loadCategories();

	// Private save functions to overwrite disk contents
	void saveCustomers();
	void saveInventories();
	void saveOrders();

	// Private helper function to split a line into sub strings with semi-colons as the delimiter
	vector<string> split(string line);

	// Private helper function to tokenize an entire file line by line.
	vector<vector<string> > tokenize_file(string filename);

	// Private variables for storage
	vector<Customer> mCustomers;
	vector<Inventory> mInventories;
	vector<Order> mOrders;

	// Map of merchants, key is username
	map<string, Merchant> mMerchants;

	// Map of categories, key is Category ID
	map<int, Category> mCategories;
};

#endif /* DATABASE_H_ */