#include <iostream>
#include <iterator>
#include <fstream>
#include <stdlib.h>

#include "database.h"

using namespace std;

void Database::load()
{
	loadCustomers();
	loadInventories();
	loadOrders();
	loadMerchants();
	loadCategories();
}

void Database::save()
{
	saveCustomers();
	saveInventories();
	saveOrders();
}

int Database::checkLogin(string username, string password)
{
	if (!mCustomers.empty())
	{
		for (unsigned int i = 0; i < mCustomers.size(); i++)
			if (mCustomers[i].getUsername() == username && mCustomers[i].getPassword() == password)
				return mCustomers[i].getID();
	}

	return -1;
}

vector<Order> Database::getOrdersByCustomer(int customerID)
{
	vector<Order> v;
	for (unsigned int i = 0; i < mOrders.size(); i++)
		if (mOrders[i].getCustomerID() == customerID)
			v.push_back(mOrders[i]);

	return v;
}

vector<Order> Database::getOrdersByDate(int date)
{
	vector<Order> v;
	for (unsigned int i = 0; i < mOrders.size(); i++)
		if (mOrders[i].getDate() == date)
			v.push_back(mOrders[i]);

	return v;
}

Inventory* Database::getInventory(int SKU)
{
	for (unsigned int i = 0; i < mInventories.size(); i++)
		if (mInventories[i].getSKU() == SKU)
			return &mInventories[i];

	return 0;
}

int Database::getCustomerBalance(int customerID)
{
	for (unsigned int i = 0; i < mCustomers.size(); i++)
		if (mCustomers[i].getID() == customerID)
			return mCustomers[i].getBalance();

	return -1;
}

void Database::removeInventory(int SKU)
{
	vector<Inventory>::iterator itr = mInventories.begin();
	for (; itr != mInventories.end(); itr++)
	{
		if ((*itr).getSKU() == SKU)
		{
			mInventories.erase(itr);
			return;
		}
	}
}

bool Database::processPurchase(int customerID, int SKU, int quantity, int date)
{
	for (unsigned int i = 0; i < mCustomers.size(); i++)
	{
		if (mCustomers[i].getID() == customerID)
		{
			Customer customer = mCustomers[i];
			Inventory* inv = getInventory(SKU);

			// Check if SKU exists, check if it exists for that quantity, 
			// check if user has money for that quantity * price, update user balance, 
			// update inventory quantity, add order object
			if (inv == 0 || (inv->getQuantity() < quantity))
			{
				cout << "ERROR: Either the SKU does not exist or there is not enough in stock." << endl;
				return false;
			}

			// Get total cost of everything
			int totalCost = inv->getPrice() * quantity;

			if (customer.getBalance() < totalCost)
			{
				cout << "ERROR: You do not have enough money!" << endl
					 << "       * Balance: " << customer.getBalance() << endl
					 << "       * Cost: " << totalCost << endl;
				return false;
			}

			// Simualte the purchase (updates user balance internally)
			customer.purchase(totalCost);

			// Update the inventory quantity
			inv->setQuantity(inv->getQuantity() - quantity);

			// Get an acceptable Order ID (get current largest ID and add 1 to it)...
			// The largest ID will always be the last element on the 'mOrders' vector
			int id = mOrders.back().getID() + 1;

			Order order(id, customerID, quantity, totalCost, date, SKU);
			mOrders.push_back(order);
		}
	}
	return false;
}

void Database::updateCustomerBalance(int customerID, int newBalance)
{
	for (unsigned int i = 0; i < mCustomers.size(); i++)
	{
		if (mCustomers[i].getID() == customerID)
		{
			mCustomers[i].setBalance(newBalance);
			return;
		}
	}
}

void Database::updateInventoryUnitPrice(int SKU, int newPrice)
{
	for (unsigned int i = 0; i < mInventories.size(); i++)
	{
		if (mInventories[i].getSKU() == SKU)
		{
			mInventories[i].setPrice(newPrice);
			return;
		}
	}
}

void Database::updateInventoryQuantity(int SKU, int newQuantity)
{
	for (unsigned int i = 0; i < mInventories.size(); i++)
	{
		if (mInventories[i].getSKU() == SKU)
		{
			mInventories[i].setQuantity(newQuantity);
			return;
		}
	}
}

void Database::loadCustomers()
{
	// Tokenize the file into a 2D vector
	vector<vector<string> > file = tokenize_file("./data/customers.txt");

	// Since we know how many lines there are (which is = to # of objects) we should
	// reserve it!
	mCustomers.reserve(file.size());
	
	for (unsigned int i = 0; i < file.size(); i++)
	{
		vector<string> line = file[i];

		int id = atoi(line[0].c_str());
		string username = line[1];
		string password = line[2];
		string name = line[3];
		string address = line[4];
		string city = line[5] ;
		string state = line[6];
		int zip = atoi(line[7].c_str());
		int balance = atoi(line[8].c_str());

		Customer customer(id, username, password, name, address, city, state, zip, balance);
		
		mCustomers.push_back(customer);
	}
}

void Database::loadInventories()
{
	// Tokenize the file into a 2D vector
	vector<vector<string> > file = tokenize_file("./data/inventory.txt");

	// Since we know how many lines there are (which is = to # of objects) we should
	// reserve it!
	mInventories.reserve(file.size());
	
	for (unsigned int i = 0; i < file.size(); i++)
	{
		vector<string> line = file[i];

		int categoryID = atoi(line[0].c_str());
		int price = atoi(line[1].c_str());
		int quantity = atoi(line[2].c_str());
		int SKU = atoi(line[3].c_str());
		string name = line[4];
		string description = line[5];

		Inventory inventory(categoryID, price, quantity, SKU, name, description);
		mInventories.push_back(inventory);
	}
}

void Database::loadOrders()
{
	// Tokenize the file into a 2D vector
	vector<vector<string> > file = tokenize_file("./data/orders.txt");

	// Since we know how many lines there are (which is = to # of objects) we should
	// reserve it!
	mOrders.reserve(file.size());
	
	for (unsigned int i = 0; i < file.size(); i++)
	{
		vector<string> line = file[i];

		int id = atoi(line[0].c_str());
		int customerID = atoi(line[1].c_str());
		int quantity = atoi(line[2].c_str());
		int price = atoi(line[3].c_str());
		int date = atoi(line[4].c_str());
		int SKU = atoi(line[5].c_str());

		Order order(id, customerID, quantity, price, date, SKU);
		mOrders.push_back(order);
	}
}

void Database::loadMerchants()
{
	// Tokenize the file into a 2D vector
	vector<vector<string> > file = tokenize_file("./data/merchant.txt");
	
	for (unsigned int i = 0; i < file.size(); i++)
	{
		vector<string> line = file[i];

		// Constructor parameters to make the object
		string username = line[0];
		string password = line[1];

		Merchant merchant(username, password);
		mMerchants[username] = merchant;
	}
}

void Database::loadCategories()
{
	// Tokenize the file into a 2D vector
	vector<vector<string> > file = tokenize_file("./data/category.txt");
	
	for (unsigned int i = 0; i < file.size(); i++)
	{
		vector<string> line = file[i];

		int id = atoi(line[0].c_str());
		string name = line[1];

		Category category(id, name);
		mCategories[id] = category;
	}
}

void Database::saveCustomers()
{
	ofstream f("./data/customers.txt");
	
	if (!f.is_open())
	{
		std::cout << "ERROR: Couldn't open file for writing in saveCustomers()" << endl;
		return;
	}

	for (unsigned int i = 0; i < mCustomers.size(); i++)
		f << mCustomers[i].lineify() << endl;

	f.close();
}

void Database::saveInventories()
{
	ofstream f("./data/inventory.txt");

	if (!f.is_open())
	{
		std::cout << "ERROR: Couldn't open file for writing in saveCustomers()" << endl;
		return;
	}

	for (unsigned int i = 0; i < mInventories.size(); i++)
		f << mInventories[i].lineify() << endl;

	f.close();
}

void Database::saveOrders()
{
	ofstream f("./data/orders.txt");

	if (!f.is_open())
	{
		std::cout << "ERROR: Couldn't open file for writing in saveCustomers()" << endl;
		return;
	}

	for (unsigned int i = 0; i < mOrders.size(); i++)
		f << mOrders[i].lineify() << endl;

	f.close();
}

vector<string> Database::split(string line)
{
	// Initialize some variables...
	vector<string> v;
	string tmp;
	string::iterator itr = line.begin();

	for (; itr != line.end(); ++itr)
	{
		if (*itr == ';')
		{
			// Add this token to the vector
			v.push_back(tmp);

			// Clear our temporary string object and erase this token (+ delimiter) from 'line'
			tmp.clear();
			line.erase(tmp.begin(), itr);

			// Reset our iterator to the new beginning of 'line' and then jump to next loop cycle
			itr = line.begin();
			continue;
		}

		// Default loop scenario is to just push each character onto the temporary string object
		tmp.push_back(*itr);
	}

	return v;
}

vector<vector<string> > Database::tokenize_file(string filename)
{
	// Open the filename
	ifstream f(filename.c_str());
	string line;

	vector<vector<string> > tokenized;

	if (f.is_open())
	{
		while (f.good())
		{
			// Grab the next line
			getline(f, line);

			if (line.size() == 0)
				return tokenized;

			// Add the split strings to the vector of tokenized lines (the tokenized file)
			tokenized.push_back(split(line));
		}

		// Done with file, close the stream
		f.close();
	}
	else
		std::cout << "ERROR: Could not open " << filename << " for loading." << std::endl;

	return tokenized;
}