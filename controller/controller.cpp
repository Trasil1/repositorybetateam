#include "controller.h"
#include "../view/view.h"

Controller::Controller()
{
	mView = new View(this);
}

void Controller::run()
{
	mModel.init();
	mView->init();
	delete mView;
	mModel.save();
}