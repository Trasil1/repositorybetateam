#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "../model/model.h"

// Forward declaration necessary to prevent infinite memory allocation.
class View;

class Controller
{
public:
	Controller();

	void run();

	// Wrappers
	void registerCustomer(Customer customer) { mModel.registerCustomer(customer); }
	bool checkLogin(string username, string password) { return mModel.checkLogin(username, password); }
	vector<Inventory> getInventories() { return mModel.getInventories(); }
	string getCategoryName(int id) { return mModel.getCategoryName(id); }
	void updateCustomerBalance(int newBalance) { mModel.updateCustomerBalance(newBalance); }
	vector<Order> getOrders() { return mModel.getOrders(); }
	bool purchase(int SKU, int quantity) { return mModel.purchase(SKU, quantity); }

	// Wrappers for Merchant
	bool checkMerchantLogin(string username, string password) { return false; }

	void updateInventory(int SKU, int price, int quantity) { mModel.updateInventory(SKU, price, quantity); }
	void removeInventory(int SKU) { mModel.removeInventory(SKU); }

	vector<Order> getOrdersByDate(int date) { return mModel.getOrdersByDate(date); }
	vector<Order> getOrdersByCID(int cid) { return mModel.getOrdersByCID(cid); }

	map<int,Category> getCategories() { return mModel.getCategories(); }

	void addInventory(Inventory inventory) { mModel.addInventory(inventory); }

private:
	// View and Model objects
	View* mView;
	Model mModel;
};

#endif