#Compiler
CC=g++

#Compiler flags
CFLAGS=-c -Wall

#Core
CORE_SRCS = core/category.cpp core/customer.cpp core/inventory.cpp core/merchant.cpp core/order.cpp
CORE_OBJS = $(CORE_SRCS:.cpp=.o)

#Model
MODEL_SRCS = model/database.cpp model/model.cpp
MODEL_OBJS = $(MODEL_SRCS:.cpp=.o)

#View
VIEW_SRCS = view/view.cpp
VIEW_OBJS = $(VIEW_SRCS:.cpp=.o)

#Controller
CONT_SRCS = controller/controller.cpp
CONT_OBJS = $(CONT_SRCS:.cpp=.o)

all: inventory

inventory: $(CORE_OBJS) $(MODEL_OBJS) $(CONT_OBJS) $(VIEW_OBJS) main.o
	$(CC) $^ -o $@

main.o: main.cpp
	$(CC) $(CFLAGS) $^ -o $@

core/%.o: core/%.cpp
	$(CC) $(CFLAGS) $^ -o $@

model/%.o: model/%.cpp
	$(CC) $(CFLAGS) $^ -o $@

view/%.o: view/%.cpp
	$(CC) $(CFLAGS) $^ -o $@

controller/%.o: controller/%.cpp
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -rf inventory core/*.o model/*.o view/*.o controller/*.o main.o