#ifndef CUSTOMER_H_
#define CUSTOMER_H_

#include <string>
#include <sstream>
#include "storable.h"

using std::string;
using std::stringstream;

// Class to hold Customer information and interact with variables.
class Customer : public Storable
{
public:
    static const int NONE = -1;

    // Defualt constructor & specific constructor.
    Customer();
    Customer(int id, string user, string pw, string name, string addr,
                       string city, string state, int zip, int bal);
    
    // Method to deposit a certain amount into your bank account.
    void deposit(int amt) { mBalance += amt; }

    // Method to simulate the purchase of something
    // NOTE: This method only simulates this for the customer,
    //       all categorizing of products in the databasing is handled outside of this class.
    bool purchase(int price);
    
    // Accessors
    int getID()          { return mID; }
    string getUsername() { return mUsername; }
    string getPassword() { return mPassword; }
    string getName()     { return mName; }
    string getAddress()  { return mAddress; }
    string getCity()     { return mCity; }
    string getState()    { return mState; }
    int getZip()         { return mZip; }
    int getBalance()     { return mBalance; }
    
    // Mutators
    void setID(const int id)                { mID = id; }
    void setUsername(const string username) { mUsername = username; }
    void setPassword(const string password) { mPassword = password; }
    void setName(const string name)         { mName = name; }
    void setAddress(const string address)   { mAddress = address; }
    void setCity(const string city)         { mCity = city; }
    void setState(const string state)       { mState = state; }
    void setZip(const int zip)              { mZip = zip; }
    void setBalance(const int bal)          { mBalance = bal; }

    // Inherited
    virtual string lineify()
    {
        stringstream ss;
        ss << mID << ";" << mUsername << ";" << mPassword << ";" << mName << ";" << mAddress << ";"
           << mCity << ";" << mState << ";" << mZip << ";" << mBalance << ";";
        return ss.str();
    }
    
private:
    // Specific account information
    int mID;
    string mUsername;
    string mPassword;

    // Personal information
    string mName;
    string mAddress;
    string mCity;
    string mState;
    int mZip;

    // Statement stuff
    int mBalance;    
};

#endif /* CUSTOMER_H_ */