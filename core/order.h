#ifndef ORDER_H_
#define ORDER_H_

#include <string>
#include <sstream>
#include "storable.h"

using std::string;
using std::stringstream;

// Class to hold Order information loaded from the database files
class Order : public Storable
{
public:
    static const int NONE = -1;

    // Constructors
    Order();
    Order(int id, int customerID, int quantity, int price, int date, int sku);

    // Accessors
    int getID()         { return mID; }
    int getCustomerID() { return mCustomerID; }
    int getQuantity()   { return mQuantity; }
    int getPrice()      { return mPrice; }
    int getDate()       { return mDate; }
    int getSKU()        { return mSKU; }

    // Mutators
    void setID(int id)                 { mID = id; }
    void setCustomerID(int customerID) { mCustomerID = customerID; }
    void setQuantity(int quantity)     { mQuantity = quantity; }
    void setPrice(int price)           { mPrice = price; }
    void setDate(int date)             { mDate = date; }
    void setSKU(int sku)               { mSKU = sku; }

    // Inherited
    virtual string lineify()
    {
        stringstream ss;
        ss << mID << ";" << mCustomerID << ";" << mQuantity << ";" << mPrice << ";"
           << mDate << ";" << mSKU << ";";
        return ss.str();
    }

private:
    int mID, mCustomerID, mQuantity, mPrice, mDate, mSKU;
};

#endif /* ORDER_H_ */