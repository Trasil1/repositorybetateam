#ifndef MERCHANT_H_
#define MERCHANT_H_

#include <string>
#include <sstream>
#include "storable.h"

using std::string;
using std::stringstream;

// Class to hold Merchant login information
class Merchant
{
public:
	// Constructors
	Merchant() { }
	Merchant(string username, string password)
	{
		mUsername = username;
		mPassword = password;
	}

	// Accessors
	string getUsername() { return mUsername; }
	string getPassword() { return mPassword; }

	// Mutators
	void setUsername(string username) { mUsername = username; }
	void setPassword(string password) { mPassword = password; }

private:
	string mUsername, mPassword;
};

#endif /* MERCHANT_H_ */