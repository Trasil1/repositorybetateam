#include "order.h"

Order::Order()
{
	mID = NONE;
	mCustomerID = NONE;
	mQuantity = NONE;
	mPrice = NONE;
	mDate = NONE;
	mSKU = NONE;
}

Order::Order(int id, int customerID, int quantity, int price, int date, int sku)
{
	mID = id;
	mCustomerID = customerID;
	mQuantity = quantity;
	mPrice = price;
	mDate = date;
	mSKU = sku;
}