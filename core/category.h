#ifndef CATEGORY_H_
#define CATEGORY_H_

#include <string>
#include <sstream>

#include "storable.h"

using std::string;
using std::stringstream;

class Category
{
public:
	static const int NONE = -1;

	// Constructors
	Category() { mID = NONE; }
	Category(int id, string name)
	{
		mID = id;
		mName = name;
	}

	// Accessors
	int getID()      { return mID; }
	string getName() { return mName; }

	// Mutators
	void setID(const int id)        { mID = id; }
	void setName(const string name) { mName = name; }

private:
	int mID;
	string mName;
};

#endif