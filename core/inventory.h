#ifndef INVENTORY_H_
#define INVENTORY_H_

#include <string>
#include <sstream>
#include "storable.h"

using std::string;
using std::stringstream;

// Class to hold Inventory information
class Inventory : public Storable
{
public:
	static const int NONE = -1;

	// Constructors
	Inventory() 
	{
		mCategoryID = NONE;
		mPrice = NONE;
		mQuantity = NONE;
		mSKU = NONE;
	}

	Inventory(int categoryID, int price, int quantity, int SKU, string name, string desc)
	{
		mCategoryID = categoryID;
		mPrice = price;
		mQuantity = quantity;
		mSKU = SKU;
		mName = name;
		mItemDescription = desc;
	}

	// Accessors
	int getCategoryID() { return mCategoryID; }
	int getPrice()      { return mPrice; }
	int getQuantity()   { return mQuantity; }
	int getSKU()        { return mSKU; }

	string getName()        { return mName; }
	string getDescription() { return mItemDescription; }

	// Mutators
	void setCategoryID(int categoryID) { mCategoryID = categoryID; }
	void setPrice(int price)           { mPrice = price; }
	void setQuantity(int quantity)     { mQuantity = quantity; }
	void setSKU(int sku)               { mSKU = sku; }

	void setName(string name)        { mName = name; }
	void setDescription(string desc) { mItemDescription = desc; }

	// Inherited
	virtual string lineify()
	{
		stringstream ss;
		ss << mCategoryID << ";" << mPrice << ";" << mQuantity << ";" << mSKU << ";" 
		   << mName << ";" << mItemDescription << ";";
		return ss.str();
	}

private:
	int mCategoryID, mPrice, mQuantity, mSKU;
	string mName, mItemDescription;
};

#endif /* INVENTORY_H_ */
