#ifndef STORABLE_H_
#define STORABLE_H_

#include <string>

using std::string;

// Class to be inherited by any object that represents a line in a database file.
class Storable
{
	// 'Lineify' 's the derived object so it may be stored in the database files.
	virtual string lineify() = 0;
};

#endif