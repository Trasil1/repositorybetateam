#include "customer.h"

Customer::Customer()
{
    // We only need to initialize integers since std::string's default constructor automatically
    // initializes strings to "".
    mID = NONE;
    mZip = NONE;
    mBalance = NONE;
}

Customer::Customer(int id, string user, string pw, string name, string addr,
                       string city, string state, int zip, int bal)
{
    mID = id;
    mUsername = user;
    mPassword = pw;
    mName = name;
    mAddress = addr;
    mCity = city;
    mState = state;
    mZip = zip;
    mBalance = bal;
}

bool Customer::purchase(const int amt)
{
    if (mBalance - amt < 0)
        return false;

    mBalance -= amt;
    return true;
}