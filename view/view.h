#ifndef VIEW_H_
#define VIEW_H_

#include <string>

// Forward declaration necessary to prevent infinite memory allocation.
class Controller;

using namespace std;

class View
{
public:
	// Constructor
	View(Controller* controller) { mController = controller; }

	// Show initial menu
	void init();

private:
	// Private helper functions to handle the different menus
	void customerUI();
	void merchantUI();

	// Customer UI sub-menus
	void customer_loginUI();
	void customer_registerUI();

	// Customer post-login menu
	void customer_postUI();

	// Customer post-login sub-menus and actions
	void customer_showStoreUI();
	void customer_purchaseUI();
	void customer_pastOrdersUI();
	void customer_changeBalUI();

	// Merchant UI sub-menus
	void merchant_postUI();

	// Merchant post-login sub-menus
	void merchant_listInv();
	void merchant_updateInv();
	void merchant_removeInv();
	void merchant_ordersByDate();
	void merchant_ordersByCID();
	void merchant_addInv();

	// A direct line to the controller
	Controller* mController;
};

#endif /* VIEW_H_ */