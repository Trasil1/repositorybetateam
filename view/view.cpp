#include <iostream>

#include "view.h"
#include "../controller/controller.h"

#include "../core/customer.h"

using namespace std;

void View::init()
{
	int op = 0;
	cout << "=== Welcome!" << endl
		 << "1) Customer"  << endl
		 << "2) Merchant"  << endl
		 << "3) Exit"      << endl << endl
		 << "Please type the digit for the menu option you wish to select and then press enter." << endl << endl;
	
	// Get the selected option
	cout << ">";
	cin >> op;

	// If it isn't acceptable input, tell them and re-run this function
	if (op != 1 && op != 2 && op != 3)
	{
		cout << "ERROR: That isn't an acceptable menu option!" << endl;
		return init();
	}

	if (op == 1)
		return customerUI();
	else if (op == 2)
		return merchantUI();
	else if (op == 3)
		return;
}

void View::customerUI()
{
	int op = 0;
	cout << "=== Customer" << endl
		 << "1) Login"     << endl
		 << "2) Register"  << endl << endl;

	cout << ">";
	cin >> op;

	if (op != 1 && op != 2)
	{
		cout << "ERROR: That isn't an acceptable menu option!" << endl;
		return customerUI();
	}

	if (op == 1)
		return customer_loginUI();
	else if (op == 2)
		return customer_registerUI();
}

void View::merchantUI()
{
	string username, password;
	cout << "=== Merchant" << endl
		 << "Username: ";
	cin >> username;
	cout << "Password: ";
	cin >> password;

	if (!mController->checkMerchantLogin(username, password))
	{
		cout << "ERROR: Incorrect login information!" << endl;

		// We return them to the home screen instead of asking them for
		// login information again just in case there aren't any merchants
		// in order to prevent an infinite loop of asking for input.
		return init();
	}
	return merchant_postUI();
}

void View::customer_loginUI()
{
	string username, password;
	cout << "=== Login" << endl;
	cout << "Username: ";
	cin >> username;
	cout << "Password: ";
	cin >> password;

	if (!mController->checkLogin(username, password))
	{
		// Let 'em know no user exists and then return to home customer UI
		cout << "ERROR: Incorrect login information!" << endl;
		return customerUI();
	}
	return customer_postUI();
}

void View::customer_registerUI()
{
	// ID is -1, this gets updated before insertion into the vector in database.h
	int id = -1;
	string username, password, name, address, city, state;
	int zip = 0;
	int balance = 0;

	cout << "=== Register" << endl;
	cout << "Username: ";
	cin >> username;
	cout << "Password: ";
	cin >> password;
	cout << "Full Name: ";
	cin.get();
	getline(cin, name);
	cout << "Address: ";
	cin.get();
	getline(cin, address);
	cout << "City: ";
	cin >> city;
	cout << "State: ";
	cin >> state;
	cout << "Zip: ";
	cin >> zip;
	cout << "Initial Balance: ";
	cin >> balance;

	// Register
	Customer customer(id, username, password, name, address, city, state, zip, balance);
	mController->registerCustomer(customer);
	
	// Return them to customer home screen
	return customerUI();
}

void View::customer_postUI()
{
	int op = 0;
	cout << "=== Store" << endl
		 << "1) Show items for sale." << endl
		 << "2) Purchase." << endl
		 << "3) View past orders." << endl
		 << "4) Change balance." << endl
		 << "5) Exit." << endl << endl;

	cout << ">";
	cin >> op;

	if (op < 1 || op > 5)
	{
		cout << "ERROR: Invalid option!" << endl;
		return customer_postUI();
	}

	switch (op)
	{
		case 1:
			customer_showStoreUI();
			break;

		case 2:
			customer_purchaseUI();
			break;

		case 3:
			customer_pastOrdersUI();
			break;

		case 4:
			customer_changeBalUI();
			break;

		case 5:
			return;

		default:
			cout << "ERROR: An unexpected error was encountered." << endl;
			break;
	}
	
	return customer_postUI();
}

void View::customer_showStoreUI()
{
	vector<Inventory> v = mController->getInventories();
	
	if (v.empty())
	{
		cout << "*** No items ***" << endl;
		return;
	}

	for (unsigned int i = 0; i < v.size(); i++)
	{
		Inventory inv = v[i];
		cout << "Name: " << inv.getName() << endl
			 << "Description: " << inv.getDescription() << endl
			 << "Category: " << mController->getCategoryName(inv.getCategoryID()) << endl
			 << "Price Per Unit: " << inv.getPrice() << endl
			 << "Stock: " << inv.getQuantity() << endl
			 << "SKU: " << inv.getSKU() << endl << endl;
	}
}

void View::customer_purchaseUI()
{
	int SKU = 0;
	int quantity = 0;

	// Print a list of all the items in the store first
	customer_showStoreUI();

	// This is hacky but it's redundant to continue if there isn't anything to buy
	vector<Inventory> v = mController->getInventories();
	
	if (v.empty())
		return;

	cout << "=== Purchase" << endl
		 << "SKU: ";
	cin >> SKU;
	cout << "Quantity: ";
	cin >> quantity;

	if (mController->purchase(SKU, quantity))
		cout << "*** Order Placed ***" << endl;
}

void View::customer_pastOrdersUI()
{
	vector<Order> v = mController->getOrders();

	if (v.empty())
	{
		cout << "*** No items ***" << endl;
		return;
	}

	for (unsigned int i = 0; i < v.size(); i++)
	{
		Order o = v[i];
		cout << "SKU: " << o.getSKU() << endl
			 << "Quantity: " << o.getQuantity() << endl
			 << "Cost: " << o.getPrice() << endl
			 << "Date: " << o.getDate() << endl << endl;
	}
}

void View::customer_changeBalUI()
{
	int bal = 0;
	cout << "=== Update Balance" << endl
		 << "New Balance: ";
	cin >> bal;
	mController->updateCustomerBalance(bal);
}

void View::merchant_postUI()
{
	int op = 0;
	cout << "=== Merchant" << endl
		 << "1) List inventory" << endl
		 << "2) Update existing inventory" << endl
		 << "3) Remove inventory" << endl
		 << "4) Display orders by date" << endl
		 << "5) Display orders by customer ID" << endl
		 << "6) Add Inventory" << endl
		 << "7) Exit" << endl << endl;

	cout << ">";
	cin >> op;

	if (op < 1 || op > 5)
	{
		cout << "ERROR: Invalid option!" << endl;
		return merchant_postUI();
	}

	switch (op)
	{
		case 1:
		    merchant_listInv();
			break;

		case 2:
			merchant_updateInv();
			break;

		case 3:
			merchant_removeInv();
			break;

		case 4:
			merchant_ordersByDate();
			break;

		case 5:
			merchant_ordersByCID();
			break;

		case 6:
			merchant_addInv();
			break;

		case 7:
			return;

		default:
			cout << "ERROR: An unexpected error was encountered.";
			break;
	}
	return merchant_postUI();
}

void View::merchant_listInv()
{
	vector<Inventory> v = mController->getInventories();
	
	if (v.empty())
	{
		cout << "*** No items ***" << endl;
		return;
	}

	for (unsigned int i = 0; i < v.size(); i++)
	{
		Inventory inv = v[i];
		cout << "Name: " << inv.getName() << endl
			 << "Description: " << inv.getDescription() << endl
			 << "Category: " << mController->getCategoryName(inv.getCategoryID()) << endl
			 << "Price Per Unit: " << inv.getPrice() << endl
			 << "Stock: " << inv.getQuantity() << endl
			 << "SKU: " << inv.getSKU() << endl << endl;
	}
}

void View::merchant_updateInv()
{
	int SKU = 0;
	int price = 0;
	int quantity = 0;

	// Print a list of all the items in the store first
	merchant_listInv();

	// This is hacky but it's redundant to continue if there isn't anything to buy
	vector<Inventory> v = mController->getInventories();
	
	if (v.empty())
		return;

	cout << "=== Update" << endl
		 << "SKU: ";
	cin >> SKU;
	cout << "New Quantity: ";
	cin >> quantity;
	cout << "New Price: ";
	cin >> price;

	mController->updateInventory(SKU, price, quantity);
}

void View::merchant_removeInv()
{
	int SKU = 0;

	// Print a list of all the items in the store first
	merchant_listInv();

	// This is hacky but it's redundant to continue if there isn't anything to buy
	vector<Inventory> v = mController->getInventories();
	
	if (v.empty())
		return;

	cout << "=== Remove" << endl
		 << "SKU: ";
	cin >> SKU;

	mController->removeInventory(SKU);
}

void View::merchant_ordersByDate()
{
	int date = 0;
	cout << "=== Orders By Date" << endl
		 << "Date: ";
	cin >> date;

	vector<Order> v = mController->getOrdersByDate(date);

	if (v.empty())
	{
		cout << "*** No orders ***" << endl;
		return;
	}

	for (unsigned int i = 0; i < v.size(); i++)
	{
		Order o = v[i];
		cout << "Customer ID: " << o.getCustomerID() << endl
			 << "SKU: " << o.getSKU() << endl
			 << "Quantity: " << o.getQuantity() << endl
			 << "Cost: " << o.getPrice() << endl
			 << "Date: " << o.getDate() << endl << endl;
	}
}

void View::merchant_ordersByCID()
{
	int cid = 0;
	cout << "=== Orders By Customer ID" << endl
		 << "ID: ";
	cin >> cid;

	vector<Order> v = mController->getOrdersByCID(cid);

	if (v.empty())
	{
		cout << "*** No orders ***" << endl;
		return;
	}

	for (unsigned int i = 0; i < v.size(); i++)
	{
		Order o = v[i];
		cout << "Customer ID: " << o.getCustomerID() << endl
			 << "SKU: " << o.getSKU() << endl
			 << "Quantity: " << o.getQuantity() << endl
			 << "Cost: " << o.getPrice() << endl
			 << "Date: " << o.getDate() << endl << endl;
	}
}

void View::merchant_addInv()
{
	map<int,Category> categories = mController->getCategories();
	if (categories.empty())
	{
		cout << "ERROR: Please close the application and manually add some categories first!" << endl;
		return;
	}
	
	map<int,Category>::iterator itr = categories.begin();
	cout << "=== Categories" << endl;
	for (; itr != categories.end(); itr++)
	{
		cout << "ID: " << itr->second.getID() << endl
			 << "Name: " << itr->second.getName() << endl << endl;
	}


	int cid = 0;
	int price = 0;
	int quantity = 0;
	int SKU = 0;
	string name, desc;

	cout << "=== Add Inventory" << endl
		 << "Category ID: ";
	cin >> cid;
	cout << "Price: ";
	cin >> price;
	cout << "Quantity: ";
	cin >> quantity;
	cout << "SKU: ";
	cin >> SKU;
	cout << "Name: ";
	cin.get();
	getline(cin, name);
	cout << "Description: ";
	cin.get();
	getline(cin, desc);

	Inventory inv(cid, price, quantity, SKU, name, desc);
	mController->addInventory(inv);	
}